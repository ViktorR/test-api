<?php
session_start();

/*
 * Инструкция для вывода ошибок и их скрытие на проде
 */

ini_set('error_reporting', E_ALL);
if(PHP_OS == 'Darwin') {
    define("HOST", "LOCAL");
    ini_set('display_errors', 1);
}
else {
    define("HOST", "PROD");
    ini_set('display_errors', 0);
}

define('ROOTDIR', dirname(__FILE__));
date_default_timezone_set("Asia/Yakutsk");

/*
 * Иногда математические функции косячат, ниже решение
 */

if (version_compare(phpversion(), "7.1", ">=")) {
    ini_set("precision", 17);
    ini_set("serialize_precision", -1);
}

require_once ROOTDIR.'/core/router.php';

