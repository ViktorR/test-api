<?php
require ROOTDIR."/models/car_models.php";

class Api_v1Controller
{

    /**
     * Получение тела запроса от клиента
     *
     * @return array объект, декодированный из json, либо смотрим в POST
     */

    private function getArrayFromJson() {
        $object = file_get_contents('php://input');
        $object = str_replace(['	'], '', $object);

        $object = json_decode($object,true);

        if ($object === null) {
            $object = $_POST;
        }
        return $object;
    }

    /**
     * Возврат ответа в формате json
     *
     * @param bool $return
     * @param string $message   текст сообщения
     * @param array $data   массив данных (опционально)
     *
     */

    private function generateResponse($return,$message,$data = null) {
        $response = [
            'result' => (int)$return,
            'resultMessage' => $message,
        ];
        if ($data !== null) {
            $data = ['data' => $data];
            $response = $response+$data;
        }
        echo json_encode($response,JSON_UNESCAPED_SLASHES);
        die();
    }


    /**
     * Функция добавления автомобиля
     *
     * @var array $data = [
     *              'data' = [
     *                  'name'        string Название авто
     *                  'year'       int Год выпуска
     *                  'color'       string Цвет авто
     *              ],
     *            ];
     *
     */

    public function actionCarAdd() {
        $data = $this->getArrayFromJson();

        $carsModelModel = new carModelsModel();
        list($result,$message) = $carsModelModel->add($data['data']);
        $this->generateResponse($result,$message);
    }

    /**
     * Функция редактирования автомобиля
     *
     * @var array $data = [
     *              'id' int идентификатор в БД
     *              'data' = [
     *                  'name'        string Название авто
     *                  'year'       int Год выпуска
     *                  'color'       string Цвет авто
     *              ],
     *            ];
     *
     */

    public function actionCarEdit() {
        $data = $this->getArrayFromJson();

        $carsModelModel = new carModelsModel();
        list($result,$message) = $carsModelModel->edit($data['id'],$data['data']);
        $this->generateResponse($result,$message);
    }

    /**
     * Функция удаления автомобиля
     *
     * @var array $data = [
     *              'id' int идентификатор в БД
     *            ];
     *
     */

    public function actionCarDelete() {
        $data = $this->getArrayFromJson();

        $carsModelModel = new carModelsModel();
        list($result,$message) = $carsModelModel->delete($data['id']);
        $this->generateResponse($result,$message);
    }

    /**
     * Функция запроса списка автомобилей
     *
     * @var string $_GET['order']  (опционально) поле для сортировки данных
     * @var int $_GET['limit']  (опционально) кол-во данных в ответе
     * @var int $_GET['offset'] (опционально) с какой строки вернуть данных
     *
     */

    public function actionCarList() {
        $carsModelModel = new carModelsModel();
        list($result,$message,$data) = $carsModelModel->get($_GET['order'],$_GET['limit'],$_GET['offset']);
        $this->generateResponse($result,$message,$data);
    }



}