<?php

abstract class dbWorker {

    private $host = "localhost";
    private $name = "indriver";

    private $password = "root";
    private $user = "root";

    private $db;
    private $stmt;

    public function __construct() {
        $this->pdoConnect();
    }

    protected function pdoConnect() {
        try {
            $this->db = new PDO("mysql:host=$this->host; dbname=$this->name", $this->user, $this->password);
            $this->db->query("set names utf8");
        }
        catch (PDOException $e) {
        }
    }
    protected function beginTransaction() {
        return $this->db->beginTransaction();
    }

    protected function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    protected function cancelTransaction() {
        return $this->db->rollBack();
    }

    protected function now() {
        return date("Y-m-d H:i:s");
    }

    protected function endTransaction() {
        return $this->db->commit();
    }

    protected function execute() {
        return $this->stmt->execute();
    }

    protected function lastInsertId() {
        return $this->db->lastInsertId();
    }

    protected function limitQuery($query, $offset, $count) {
        if ($offset !== "" && $count !== "") {
            $offset = intval($offset);
            $count = intval($count);
            if (is_int($offset) && is_int($count)) {
                $query .= " limit $offset, $count";
            }
        }
        return $query;
    }

    protected function query($query, $emu = true) {
        $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, $emu);
        $this->stmt = $this->db->prepare($query);
    }

    protected function resultSet($noFetch = null) {
        $this->execute();
        if (!empty($noFetch)) {
            return $this->stmt;
        }
        else {
            return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    protected function rowCount() {
        return $this->stmt->rowCount();
    }

    protected function single() {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }
}
