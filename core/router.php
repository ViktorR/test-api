<?php
/*
 * Подгружаем файлы, взятые из composer
 */
require_once ROOTDIR.'/vendor/autoload.php';

/*
 * Ниже описание роутов, методов и их направление
 */

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {

    $r->addRoute('GET', '/api/auto/v1/list', 'api_v1/CarList');
    $r->addRoute('POST', '/api/auto/v1/add', 'api_v1/CarAdd');
    $r->addRoute('POST', '/api/auto/v1/edit', 'api_v1/CarEdit');
    $r->addRoute('DELETE', '/api/auto/v1/delete', 'api_v1/CarDelete');

});

/*
 * Парсим запрос, подключаем нужные файлы и вызываем функции
 */

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        http_response_code(404);
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        http_response_code(405);
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        $segments = explode('/', $handler);
        $arr_shift = array_shift($segments);

        require (ROOTDIR.'/controllers/'.$arr_shift. '.php');

        $controllerName = $arr_shift. 'Controller';
        $controllerName = ucfirst($controllerName);
        $controllers = new $controllerName;

        $actionName = 'action' . ucfirst($segments[0]);

        $result = call_user_func_array(array($controllers, $actionName), $vars);

        if ($result != null) {
            break;
        }
        break;
}