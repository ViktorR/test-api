<?php
require_once ROOTDIR."/core/dbw.php";

class carModelsModel extends dbWorker
{

    /**
     * Функция добавления автомобиля
     *
     * @param array $data = [
     *               'name'        string Название авто
     *               'year'       int Год выпуска
     *               'color'       string Цвет авто
     *            ];
     *
     * @return array [bool, string]
     *
     */

    public function add($data) {
        if (empty($data['name']) || empty($data['year']) || empty($data['color'])) {
            return [false,'Заполните все поля!'];
        }

        $this->query("insert into car_models (name,year,color) values (:name,:year,:color)");
        $this->bind(":name", $data['name']);
        $this->bind(":year", $data['year']);
        $this->bind(":color", $data['color']);
        if ($this->execute() === false) {
            return [false, 'Произошла ошибка записи!'];
        }

        return [true, 'Операция выполнена успешно!'];
    }

    /**
     * Функция редактирования автомобиля
     *
     * @param int $id ID в БД
     * @param array $data = [
     *               'name'        string Название авто
     *               'year'       int Год выпуска
     *               'color'       string Цвет авто
     *            ];
     *
     * @return array [bool, string]
     *
     */

    public function edit($id,$data) {
        if (empty($data['name']) || empty($data['year']) || empty($data['color'])) {
            return [false,'Заполните все поля!'];
        }

        $carModelData = $this->getCarById($id);

        if ($carModelData === false) {
            return [false,'Запись с данным идентификатором не найдена!'];
        }

        $this->query("update car_models set name=:name, year=:year, color=:color where id=:id");
        $this->bind(":name", $data['name']);
        $this->bind(":year", $data['year']);
        $this->bind(":color", $data['color']);
        $this->bind(":id", $id);
        if ($this->execute() === false) {
            return [false, 'Произошла ошибка записи!'];
        }

        return [true, 'Операция выполнена успешно!'];
    }

    /**
     * Функция удаления автомобиля
     *
     * @param int $id ID в БД
     *
     * @return array [bool, string]
     *
     */

    public function delete($id) {
        $carModelData = $this->getCarById($id);

        if ($carModelData === false) {
            return [false,'Запись с данным идентификатором не найдена!'];
        }

        $this->query("delete from car_models where id=:id");
        $this->bind(":id", $id);
        if ($this->execute() === false) {
            return [false, 'Произошла ошибка удаления!'];
        }

        return [true, 'Операция выполнена успешно!'];
    }

    public function get($order,$limit,$offset) {
        if ($limit === null) {
            $limit = 10;
        }
        if ($offset === null) {
            $offset = 0;
        }

        switch ($order) {
            case 'name' :
                $orderBy = 'name';
                break;
            case 'year' :
                $orderBy = 'year';
                break;
            case 'year_desc' :
                $orderBy = 'year desc';
                break;
            default :
                $orderBy = 'id';
                break;
        }

        $query = "select * from car_models order by $orderBy";
        $query = $this->limitQuery($query, $offset, $limit);
        $this->query($query);
        $array = [];

        $result = $this->resultSet();
        foreach ($result as $key=>$val) {
            $array[] = [
                'id' => (int)$val['id'],
                'name' => $val['name'],
                'year' => (int)$val['year'],
                'color' => $val['color'],
            ];
        }
        return [true, 'Ошибок нет', $array];
    }


    /**
     * Запрос модели авто по ИД
     *
     * @param $id int ID в БД
     * @return mixed либо false, либо данные
     */

    private function getCarById($id) {
        $this->query("select * from car_models where id=:id");
        $this->bind(":id", $id);
        return $this->single();
    }


}